
# License: This program is free software; you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3 of
# the License, or (at your option) any later version. This program is
# distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.

import SimPy.Simulation
import yaml


import itamae.scheduler
import itamae.system
import itamae.io

class Runner(SimPy.Simulation.Simulation):
    """\
    Main itamae test running module
    """
    

    def __init__(self, test_config):
        "Initialize simulation"

        SimPy.Simulation.Simulation.__init__(self)

        # parse test config
        config = yaml.load(open(test_config, 'r'))
        sysconfig = config['system']

        # check for external system config
        if sysconfig['input']:
           sysinput = yaml.load(open(sysconfig['input'], 'r')) 
           sysconfig = sysinput['system']

        # setup simulation
        self.initialize()
        
        self.system = itamae.system.System (self, sysconfig)
        self.graph = itamae.io.load_graph (config['graph'])
        
        # select scheduler from config
        scheduler_type = config['scheduler']['type']
        if scheduler_type == "sashimi":
            self.scheduler = itamae.scheduler.Sashimi (self, self.system, self.graph)
        elif scheduler_type == "sushi":
            self.scheduler = itamae.scheduler.Sushi (self, self.system, self.graph)
        elif schduler_type == "soba":
            self.scheduler = itamae.scheduler.Soba (self, self.system, self.graph)
        else:
            print ("\nError creating scheduler {0}\n".format(scheduler_type))
            sys.exit(1)

        self.sim_data = itamae.io.SimData(config['output'])

    def run(self):
        "Run the simulation"

        self.scheduler.build_schedule()
        
        self.activate (self.scheduler, self.scheduler.run())
        self.simulate(until=10000)

        self.sim_data.save()
        

    def add_sim_data (self, task_id, processor_id, start_time, stop_time):
        self.sim_data.add_event(task_id, processor_id, start_time, stop_time)
