# Copyright Pedro Ângelo <pangelo@void.io>

# License: This program is free software; you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3 of
# the License, or (at your option) any later version. This program is
# distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.

import random

from itamae.task import Task

def gen_scatter_gather_graph(config):

    mu = 1.0
    sigma = 0.5
    workers = 1
    model_f = random.gauss

    if config:
        workers = config.get('workers', workers)
        mu = config.get ('mu', mu)
        sigma = config.get ('sigma', sigma)
        model = config.get('model', "gauss")
    
    start = Task (task_id = 'start', 
                  execution_model = (lambda x: x, 0), 
                  affinity = ['cpu'], 
                  input_tasks = [],
                  output_tasks = [])

    finish = Task (task_id = 'finish', 
                  execution_model = (lambda x: x, 0), 
                  affinity = ['cpu'], 
                  input_tasks = [],
                  output_tasks = [])

    scatter = Task(task_id = 'scatter',
                   execution_model = (model_f, mu, sigma),
                   affinity=['cpu'],
                   input_tasks=[start],
                   output_tasks=[])
    start.output.append (scatter)

    gather = Task(task_id = 'gather',
                  execution_model = (model_f, mu, sigma),
                  affinity=['cpu'],
                  input_tasks=[],
                   output_tasks=[finish])
    finish.input.append(gather)

    for w in range(workers):
        worker = Task(task_id = "worker" + str(w),
                      execution_model = (model_f, mu, sigma),
                      affinity=['cpu', 'gpu'],
                      input_tasks=[scatter],
                      output_tasks=[gather])

        scatter.output.append(worker)
        gather.input.append(worker)

    return start

def gen_game_loop_graph(config):

    style = 'classic'
    mu = 1.0
    sigma = 0.5
    model_f = random.gauss
    
    if config:
        style = config.get('style', style)
        mu = config.get ('mu', mu)
        sigma = config.get ('sigma', sigma)
        model = config.get('model', "gauss")

    start = Task(task_id = 'start', 
                 execution_model = (lambda x: x, 0), 
                 affinity = ['cpu'], 
                 input_tasks = [],
                 output_tasks = [])
    
    sync = Task(task_id= 'sync',
                  execution_model = (lambda x: x, 0), 
                  affinity = ['cpu'], 
                  input_tasks = [],
                  output_tasks = [])

    render = Task(task_id = 'render',
                  execution_model = (model_f, mu, sigma),
                  affinity = ['gpu'],
                  input_tasks = [start],
                  output_tasks = [sync])

    start.output.append (render)
    sync.input.append(render)
    
    user_input = Task(task_id = 'user_input',
                      execution_model = (model_f, mu, sigma),
                      affinity = ['cpu'],
                      input_tasks = [start],
                      output_tasks = [])

    start.output.append (user_input)

    if style == 'classic':
        update = Task(task_id='update',
                      execution_model = (model_f, mu, sigma),
                      affinity = ['cpu'],
                      input_tasks=[user_input],
                      output_tasks=[sync])
        
        user_input.output.append (update)
        sync.input.append (update)
        
        return start

    audio = Task(task_id = 'audio',
                 execution_model = (model_f, mu, sigma),
                 affinity = ['dsp'],
                 input_tasks = [start],
                 output_tasks = [sync])
    
    start.output.append (audio)
    sync.input.append (audio)

    physics = Task(task_id = 'physics',
                   execution_model = (model_f, mu, sigma),
                   affinity = ['cpu','gpu'],
                   input_tasks = [user_input],
                   output_tasks = [sync])
    
    user_input.output.append (physics)
    sync.input.append (physics)

    ai = Task(task_id = 'ai',
              execution_model = (model_f, mu, sigma),
              affinity = ['cpu'],
              input_tasks = [user_input],
              output_tasks = [sync])
    
    user_input.output.append (ai)
    sync.input.append (ai)

    network = Task(task_id = 'network',
                   execution_model = (model_f, mu, sigma),
                   affinity = ['cpu'],
                   input_tasks = [user_input],
                   output_tasks = [sync])
    
    user_input.output.append (network)
    sync.input.append (network)

    return start

def gen_installation_graph(config):

    mu = 1.0
    sigma = 0.5
    model_f = random.gauss

    if config:
        mu = config.get ('mu', mu)
        sigma = config.get ('sigma', sigma)
        model = config.get('model', "gauss")
    
    start = Task(task_id = 'start', 
                 execution_model = (lambda x: x, 0), 
                 affinity = ['cpu'], 
                 input_tasks = [],
                 output_tasks = [])
    
    sync = Task(task_id= 'sync',
                  execution_model = (lambda x: x, 0), 
                  affinity = ['cpu'], 
                  input_tasks = [],
                  output_tasks = [])

    render = Task(task_id = 'render',
                  execution_model = (model_f, mu, sigma),
                  affinity = ['gpu'],
                  input_tasks = [start],
                  output_tasks = [sync])

    start.output.append (render)
    sync.input.append(render)

    stream = Task(task_id='video_stream',
                  execution_model = (model_f, mu, sigma),
                  affinity = ['cpu'],
                  input_tasks = [start],
                  output_tasks = [])

    start.output.append (stream)

    tracking = Task(task_id='tracking',
                    execution_model = (model_f, mu, sigma),
                    affinity = ['cpu', 'gpu'],
                    input_tasks = [start],
                    output_tasks = [])
    
    start.output.append (tracking)

    firesim = Task(task_id='fire_sim',
                   execution_model = (model_f, mu, sigma),
                   affinity = ['cpu', 'gpu'],
                   input_tasks = [tracking, stream],
                   output_tasks = [])

    tracking.output.append (firesim)
    stream.output.append (firesim)

    audio = Task(task_id='audio',
                 execution_model = (model_f, mu, sigma),
                 affinity = ['dsp'],
                 input_tasks = [firesim],
                 output_tasks = [sync])

    firesim.output.append (audio)
    sync.input.append(audio)

    return start
