# Copyright Pedro Ângelo <pangelo@void.io>

# License: This program is free software; you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3 of
# the License, or (at your option) any later version. This program is
# distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.

import SimPy.Simulation

class System(object):
    """
    Computational system resource abstraction
    """

    def __init__(self, sim, config):
        
        self.resources = []
        
        for resource in enumerate(config['resources']):
            r_id = resource[0]
            r_name = resource[1]['id']
            r_type = resource[1]['type']
            r_speedup = resource[1]['speedup']
            self.resources.append (Processor (r_id, r_name, r_type, r_speedup, sim))

class Processor(SimPy.Simulation.Process):
    """
    This class encapsulates a processor resource
    """
    
    def __init__(self, r_id, r_name, r_type, r_speedup, sim):
        SimPy.Simulation.Process.__init__(self, sim=sim)

        self.sim = sim
        self.id = r_id
        self.name = r_name
        self.type = r_type
        self.speedup = r_speedup
        self.task_queue = []
        self.event_free = SimPy.Simulation.SimEvent("processor {} free".format(r_id), sim)

    def run(self):
        "Main process execution method"
        
        while True:
            if len(self.task_queue) == 0:
                self.event_free.signal (self.id)
                yield SimPy.Simulation.passivate, self
            
            task = self.task_queue.pop()
            task_start = self.sim.now()
            task_time = task.exec_time() / self.speedup 
            task_stop = task_start + task_time
            self.sim.add_sim_data(task.id, self.name, task_start, task_stop)
            yield SimPy.Simulation.hold, self, task_time
            task.mark_done()

    def queue(self, task):
        self.task_queue.append (task)

    def estimate_completion(self, task):
        "estimate the completion time of a given task"

        return task.exec_time() / self.speedup
