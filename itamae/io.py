# Copyright Pedro Ângelo <pangelo@void.io>

# License: This program is free software; you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3 of
# the License, or (at your option) any later version. This program is
# distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.

import itamae.graph

def load_graph(config):
    
    gtype = config['type']
    
    if gtype == "scatter-gather":
        return itamae.graph.gen_scatter_gather_graph(config.get('params', {}))
    elif gtype == "game-loop":
        return itamae.graph.gen_game_loop_graph(config.get('params', {}))
    elif gtype == "installation":
        return itamae.graph.gen_installation_graph(config.get('params', {}))
    elif gtype == "file":
        return itamae.graph.load_graph(config.get('params', {}))
    else:
        return []

class SimData(object):

    sort_keys = {"task_id": 0,
                 "processor_id": 1,
                 "start_time": 2,
                 "stop_time": 3}

    def __init__(self, config):
        self.data = []
        self.type = config['type']
        self.outfile = config['file']
        self.sort_key = SimData.sort_keys[config['sort']]

    def add_event(self, task_id, processor_id, start_time, stop_time):
        self.data.append((task_id, processor_id, start_time, stop_time))

    def save (self):

        fname = self.outfile
        closefd = True 
        if fname == "stdout":
            fname = 1
            closefd = False

        with open(fname, mode="w", closefd=closefd) as output:
            if self.type == "csv":
                self.save_csv(output)
            elif self.type == "trace":
                self.save_trace(output)

    def save_csv(self, outfile):
        for row in sorted(self.data, key=(lambda x: x[self.sort_key])):
            outfile.write ("\t".join([str(i) for i in row]) + "\n")

    def save_trace(self, outfile):
        pass
