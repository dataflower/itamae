# Copyright Pedro Ângelo <pangelo@void.io>

# License: This program is free software; you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3 of
# the License, or (at your option) any later version. This program is
# distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
        
import random

class Task(object):
    """
    This class encapsulates a computational task
    """

    def __init__(self, task_id='a task', 
                 execution_model=(random.gauss, 1.0, 0.5), 
                 affinity=[], input_tasks = [], output_tasks=[]):
        "Build the task data structures"

        self.id = task_id
        self.input = input_tasks
        self.output = output_tasks
        self.affinity = affinity
        self.priority = 0
        self.model = execution_model[0]
        self.model_params = execution_model[1:]
        self.done = False

    def ready(self):
        for task in self.input:
            if not task.done:
                return False
        return True

    def has_affinity(self, processor):
        if len(self.affinity) == 0:
            return True
        elif processor.type in self.affinity:
            return True
        return False

    def exec_time(self):
        return max(self.model(*self.model_params), 0)

    def mark_done(self):
        self.done = True
