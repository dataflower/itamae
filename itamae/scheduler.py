# Copyright Pedro Ângelo <pangelo@void.io>

# License: This program is free software; you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3 of
# the License, or (at your option) any later version. This program is
# distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.

import SimPy.Simulation
import random

import itamae.system
import itamae.task

class StaticScheduler(SimPy.Simulation.Process):
    """ 
    Itamae static scheduler base class 
    
    Subclasses should override the build_schedule() method and adapt the
    __init__() method to the parameters they need.
    
    The run() method just plays out the static schedule assigning
    tasks to their previously assigned processors when they are ready,
    so it doesn't make any runtime scheduling decisions
    """

    def __init__(self, sim, system, graph):
        "Initialize data structures"
        SimPy.Simulation.Process.__init__(self, sim=sim)

        self.sim = sim
        self.system = system
        self.graph = graph
        self.task_queue = []

    def build_schedule(self):
        "Should be overriden by subclasses"
        pass

    def run(self, seed=None):
        "Run the scheduler"

        random.seed(seed)
        
        processor_events = []
        processor_state = []
        scheduled_tasks = []

        # start the processors
        for processor in self.system.resources:
            processor_events.append(processor.event_free)
            processor_state.append(False)
            self.sim.activate(processor, processor.run())

        # start scheduling tasks to processors
        while len(self.task_queue) != 0 or len(scheduled_tasks) != 0:

            # wait for processors to become available
            yield SimPy.Simulation.waitevent, self, processor_events
            
            for ev in self.eventsFired:
                p_id = ev.signalparam
                processor_state[p_id] = True

            # defer scheduling decision to subclass
            free_processor_ids = [i for i,s in enumerate(processor_state) if s == True]
            scheduled_tasks = self.schedule(free_processor_ids)
            
            for task, processor in scheduled_tasks:
                processor.queue(task)
                processor_state[processor.id] = False
                self.sim.reactivate(processor)
        
        self.sim.stopSimulation()

class Sashimi(StaticScheduler):
    """
    Simple list based scheduler

    For each available task select the next available processor
    """
    
    def __init__(self, sim, system, graph):
        StaticScheduler.__init__(self, sim, system, graph)

    def build_schedule(self):
        
        if not self.graph:
            return

        start_task = self.graph
        available_tasks = [start_task]

        i = 0
        while i != len(available_tasks):
            task = available_tasks[i]
            for child in task.output:
                if child not in available_tasks:
                    available_tasks.append (child)
            i = i+1

        self.task_queue = available_tasks

    def schedule(self, free_processor_ids):
        schedule_decision = []
        for task in self.task_queue:
            if not task.ready():
                 continue
            for p_id in free_processor_ids:
                processor = self.system.resources[p_id]
                if task.has_affinity(processor):
                    schedule_decision.append((task, processor))
                    free_processor_ids.remove(p_id)
                    break

        for t,p in schedule_decision:
            self.task_queue.remove(t)

        return schedule_decision

class Sushi(StaticScheduler):
    """
    List based scheduler using the HEFT heuristic
    """

    def __init__(self, sim, system, graph):
        StaticScheduler.__init__(self, sim, system, graph)

    def build_schedule(self):
        if not self.graph:
            return

        start_task = self.graph
        available_tasks = [start_task]

        i = 0
        while i != len(available_tasks):
            task = available_tasks[i]
            for child in task.output:
                if child not in available_tasks:
                    available_tasks.append (child)
            i = i+1

        self.task_queue = available_tasks

    def schedule(self, free_processor_ids):
        schedule_decision = []
        for task in self.task_queue:
            if not task.ready():
                 continue
            p_select = -1
            for p_id in free_processor_ids:
                processor = self.system.resources[p_id]                
                min_eft = -1
                if task.has_affinity(processor):
                    eft = processor.estimate_completion(task)
                    if min_eft < 0 or eft < min_eft:
                        min_eft = eft
                        p_select = p_id
            if p_select >= 0:
                schedule_decision.append((task, self.system.resources[p_select]))
                free_processor_ids.remove(p_select)
            
        for t,p in schedule_decision:
            self.task_queue.remove(t)

        return schedule_decision

class Soba(StaticScheduler):
    """
    Static scheduler using a simulated annealing approach
    """

    def __init__(self, sim, system, graph):
        StaticScheduler.__init__(self, sim, system, graph)

    def build_schedule(self):
        pass

    def schedule(self, available_processors):
        pass
